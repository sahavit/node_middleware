const bodyParser = require('body-parser');
const { response } = require('express');
var multer = require('multer')
var fs = require('fs');

var express = require('express');
var multerData = multer();

var app = express();

app.use(express.json())
app.use(multerData.array())
app.use(bodyParser.urlencoded({extended: true}))
app.use('/static',express.static('./Static'))

app.get('/studentsdata', function(req,res){
    fs.readFile('./data.txt', function(err,data){
        if(err){
            res.send(err)
            return
        }
        res.send(JSON.parse(data.toString()))
    })
})

function calculateAge(age){
    let ageRange =''
    if(age>=0 && age<=4)
        ageRange="Kid"
    else if(age>=5 && age<=20)
        ageRange="Child"
    else if(age>=21 && age<=50)
        ageRange="Adult"
    else if(age>=51 && age<=100)
        ageRange="Old"
    return ageRange;
}

app.post('/studentsdata', function(req,res){
    fs.readFile('./data.txt', function(err, data){
        if(err){
            res.send(err)
            return
        }
        var dataObjList = JSON.parse(data.toString())  
        dataObjList.push(req.body)
        let tempList = {
            name: req.body.name,
            age: calculateAge(req.body.age),
            gender: req.body.gender
        }
        fs.writeFile('./data.txt',JSON.stringify(dataObjList), function(){
            res.send(tempList)
        })
    })
})

app.listen(9000, function(){
    console.log("Server started")
})